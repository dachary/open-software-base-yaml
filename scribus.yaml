name: scribus
canonical:
  bugTracker:
    source: wikidata
    value: 'http://bugs.scribus.net'
  canonicalName:
    source: debian_appstream
    value: Scribus
  categories:
    - source: debian_appstream
      value: Qt
    - source: debian_appstream
      value: Graphics
    - source: debian_appstream
      value: Publishing
  license:
    source: wikidata
    value: GNU General Public License
  longDescription:
    en:
      source: debian
      value: " Scribus is an open source desktop page layout program with the aim of\n producing commercial grade output in PDF and Postscript, primarily, though\n not exclusively for Linux.\n .\n Scribus can be used for many tasks; from brochure design to newspapers,\n magazines, newsletters and posters to technical documentation. It has\n sophisticated page layout features like precision placing and rotating of text\n and/or images on a page, manual kerning of type, bezier curves polygons,\n precision placement of objects, layering with RGB and CMYK custom colors. The\n Scribus document file format is XML-based. Unlike proprietary binary file\n formats, even damaged documents, can be recovered with a simple text editor.\n .\n Scribus supports professional DTP features, such as CMYK color and a\n color management system to soft proof images for high quality color printing,\n flexible PDF creation options, Encapsulated PostScript import/export and\n creation of 4 color separations, import of EPS/PS and SVG as native vector\n graphics, Unicode text including right to left scripts such as Arabic and\n Hebrew via freetype. Graphic formats which can be placed in Scribus as images\n include PDF, Encapsulated Post Script (eps), TIFF, JPEG, PNG and XPixMap(xpm),\n and any bitmap type supported by QT4.\n .\n Printing, PDF and SVG creation are done via custom driver libraries and\n plug-ins, giving Scribus inventive features: the abilities to include\n presentation effects with PDF output, fully scriptable interactive PDF\n forms, SVG vector file output. The internal printer drivers fully support\n Level 2 and Level 3/PDF 1.4 postscript features including transparency and\n font embedding.\n .\n When run from KDE, Drag and Drop, for example from desktop to the canvas,\n is enabled. There is easy to use drag and drop scrapbook for frequently\n used items such as text blocks, pictures and custom shaped frames.\n .\n If you need to use the render frame install the texlive-latex-recommended\n (suggested).\n"
    fr:
      source: debian
      value: " Scribus est une application de mise en page (PAO) libre, dont l'objectif\n est de produire des documents de qualité professionnelle aux formats PDF\n et\n Postscript, principalement mais non exclusivement pour Linux.\n .\n Scribus peut être utilisé pour de nombreuses tâches\_: la conception de\n brochures, journaux, magazines, lettres et affiches ainsi que de la\n documentation technique. Il possède un système de mise en page\n sophistiquée\n incluant le placement précis et la rotation du texte et/ou des\n images, le réglage manuel de l'approche des caractères, les polygones à\n courbes de Bézier, le placement précis des objets, les calques et\n une gestion personnalisée des couleurs RVB et CMJN. Les documents Scribus\n sont enregistrés en XML. Contrairement aux formats de fichiers binaires\n propriétaires, même des documents endommagés peuvent être récupérés avec\n un simple éditeur de texte.\n .\n Scribus gère les fonctions PAO professionnelles\_: les couleurs en CMJN et\n un système de gestion des couleurs de l’image pour une haute qualité lors\n de l'impression, des options flexibles de création de PDF, l'import depuis et l'export vers du PostScript encapsulé et la séparation en\n 4\_couleurs, l'import d’EPS/PS et du SVG comme graphismes vectoriels\n natifs, le texte Unicode incluant les écritures de droite à gauche comme\n l'arabe et l'hébreu à l’aide de freetype. Les formats graphiques qui\n peuvent être incorporés dans Scribus en tant qu'image comprennent le PDF,\n l'EPS (Encapsulated PostScript), le TIFF, le JPEG, le PNG et le XPM\n (XPixMap) ainsi que n'importe quel type bitmap pris en charge par QT4.\n .\n L'impression et la création de PDF et SVG sont réalisées par des\n bibliothèques de pilotes et des greffons adaptés, donnant à Scribus des\n fonctionnalités ingénieuses\_: la possibilité d'inclure des effets de\n présentation avec la sortie PDF, des formulaires PDF interactifs\n entièrement programmables, la sortie en SVG. Les pilotes d'impression\n internes gèrent complètement les fonctionnalités postscript des niveaux\_2\n ou\_3 et PDF\_1.4, comme la transparence et l'inclusion de polices.\n .\n Lorsque Scribus est utilisé sous KDE, le glisser-déposer est actif (par\n exemple, du bureau au plan de travail). Cette facilité existe pour les\n éléments couramment utilisés comme les blocs de texte, les images et les\n formes personnalisées.\n .\n Si vous avez besoin de cadre de rendu, installez le paquet\n texlive-latex-recommended (suggestion).\n"
  screenshot:
    source: debian
    value: 'http://screenshots.debian.net/screenshots/s/scribus/9959_large.png'
debian:
  _source:
    name: Universal Debian Database
    description_url: 'https://wiki.debian.org/UltimateDebianDatabase'
    url: 'https://git.framasoft.org/codegouv/udd-yaml'
  name: scribus
  description:
    da:
      description: Sidelayout for åben kildekode-skrivebordet - stabil gren
      long_description: " Scribus er et sidelayoutprogram for åben kildekode-skrivebordet med det\n formål at lave kvalitet på niveau med kommercielle resultater i PDF og\n Postscript, primært, men ikke eksklusivt for Linux.\n .\n Scribus kan bruges for mange opgaver; fra design af brochurer til aviser,\n magasiner, nyhedsbreve og indlæg for teknisk dokumentation. Programmet har\n sofistikerede funktioner for sidelayout såsom præcis placering og rotation\n af tekst og/eller billeder på en side, manuel knibning af teksttypen,\n polygoner for bezierkurver, præcis placering af objekter, pålægning af lag\n med tilpassede RGB- og CMYK-farver. Scribus' dokumentfilformat er\n XML-baseret. Til forskel fra proprietære og binære filformater kan selv\n beskadigede dokumenter gendannes med et simpelt tekstbehandlingsprogram.\n .\n Scribus understøtter professionelle DTP-funktioner, såsom CMYK-farve og et\n farvehåndteringssystem til blød kvalitetssikring af billeder for\n farveudskrivning i høj kvalitet, fleksibel PDF-oprettelsesindstillinger,\n Encapsulated PostScript import/eksport og oprettelse af 4\n farveadskillelser, import af EPS/PS og SVG som standardvektorgrafik,\n Unicodetekst inklusiv højre mod venstre skrifttyper såsom arabisk og\n hebraisk via freetype. Grafikformater som kan placeres i Scribus som\n billeder inkluderer PDF, Encapsulated Post Script (eps), TIFF, JPEG, PNG\n og XPixMap(xpm) og alle bitmaptyper som understøttes af QT4.\n .\n Udskrivning, PDF og SVG-oprettelse udføres med tilpassede\n driverbiblioteker og udvidelsesmoduler, hvilket giver Scribus kreative\n funktioner: mulighed for at inkludere præsentationseffekter med PDF-\n uddata, interaktive PDF-formularer der kan skriptes, uddata for SVG-\n vektorfil. De interne printerdrivere understøtter fuldt ud postscript-\n funktioner for niveau 2 og niveau 3/PDF 1.4 inklusiv gennemsigtighed og\n indlejring af skrifttype.\n .\n Når kørt fra KDE er træk og slip - for eksempel fra skrivebord til lærred -\n  aktiveret. Der er en nem at bruge træk og slip-scrapbog for ofte brugte\n punkter såsom tekstblokke, billeder og tilpassede rammer.\n .\n Hvis du skal bruge optegningsrammen så installer pakken texlive-latex-\n recommended (suggested).\n"
    de:
      description: Open-Source-DTP-Programm - stabiler Zweig
      long_description: " Scribus ist ein Open-Source-DTP-Programm (Desktop Page Layout) mit dem Ziel\n für kommerzielle Zwecke geeignete Ausgabe in PDF- und Postscript-Format zu\n erstellen; hauptsächlich, aber nicht ausschließlich, für Linux.\n .\n Scribus kann für viele Aufgaben genutzt werden; von der Erstellung von\n Broschüren über Zeitungen, Magazine, Rundschreiben und Plakate bis hin zu\n technischen Dokumentationen. Es hat ausgeklügelte Fähigkeiten für das\n Seiten-Layout wie z.B. genaue Platzierung und Drehung von Text und/oder\n Bildern auf einer Seite, manuelles Kerning von Schriftarten, Bezierkurven,\n Polygone, genaue Platzierung von Objekten, Ebenenunterstützung mit\n spezifischen RGB- und CMYC-Farben. Das Scribus-Dokumentformat basiert auf\n XML. Anders als bei proprietären binären Dateiformaten können beschädigte\n Dokumente mit einem einfachen Texteditor wieder hergestellt werden.\n .\n Scribus bietet professionelle DTP-Fähigkeiten wie z.B. CMYK-Farben und\n ein Farbmanagementsystem für Korrekturabzüge für qualitativ hochwertigen\n Farbdruck, flexible Optionen für die Erstellung von PDF-Dateien, Import und\n Export von Encapsulated PostScript und Erzeugung von 4-Farb-Teilungen,\n Import von EPS/PS und SVG als echte Vektorgrafiken, Unicode-Text\n einschließlich von rechts nach links geschriebener Schriften wie Arabisch\n und Hebräisch über Freetype. Grafische Formate, die in Scribus eingebunden\n werden können, umfassen PDF, Encapsulated PostScript (eps), TIFF, JPEG, PNG\n und XPixMap (xpm) sowie sämtliche von QT4 unterstützten Bitmaptypen.\n .\n Druck, Erstellung von PDF und SVG werden über spezifische\n Treiberbibliotheken und Erweiterungen durchgeführt, so dass Scribus\n einfallsreiche Eigenschaften erhält: die Fähigkeit, Präsentationseffekte in\n PDF-Ausgaben einzubinden, vollständig skriptunterstützte interaktive\n PDF-Formulare, Ausgabe von SVG-Vektor-Dateien. Die internen Druckertreiber\n unterstützen vollständig Level-2- und\n Level-3/PDF-1.4-PostScript-Fähigkeiten wie Transparenz und\n Schrifteinbettung.\n .\n Wenn man Scribus von KDE aus startet, wird Drag and Drop, z.B. vom\n Desktop auf das Arbeitsblatt, unterstützt. Es gibt ein leicht zu\n bedienendes Drag-and-Drop-Notizbuch für häufig genutzte Dinge wie\n Textblöcke, Bilder und speziell gestaltete Frames.\n .\n Falls Sie den »render frame« verwenden müssen, installieren Sie\n das (vorgeschlagene) Paket texlive-latex-recommended.\n"
    en:
      description: Open Source Desktop Page Layout - stable branch
      long_description: " Scribus is an open source desktop page layout program with the aim of\n producing commercial grade output in PDF and Postscript, primarily, though\n not exclusively for Linux.\n .\n Scribus can be used for many tasks; from brochure design to newspapers,\n magazines, newsletters and posters to technical documentation. It has\n sophisticated page layout features like precision placing and rotating of text\n and/or images on a page, manual kerning of type, bezier curves polygons,\n precision placement of objects, layering with RGB and CMYK custom colors. The\n Scribus document file format is XML-based. Unlike proprietary binary file\n formats, even damaged documents, can be recovered with a simple text editor.\n .\n Scribus supports professional DTP features, such as CMYK color and a\n color management system to soft proof images for high quality color printing,\n flexible PDF creation options, Encapsulated PostScript import/export and\n creation of 4 color separations, import of EPS/PS and SVG as native vector\n graphics, Unicode text including right to left scripts such as Arabic and\n Hebrew via freetype. Graphic formats which can be placed in Scribus as images\n include PDF, Encapsulated Post Script (eps), TIFF, JPEG, PNG and XPixMap(xpm),\n and any bitmap type supported by QT4.\n .\n Printing, PDF and SVG creation are done via custom driver libraries and\n plug-ins, giving Scribus inventive features: the abilities to include\n presentation effects with PDF output, fully scriptable interactive PDF\n forms, SVG vector file output. The internal printer drivers fully support\n Level 2 and Level 3/PDF 1.4 postscript features including transparency and\n font embedding.\n .\n When run from KDE, Drag and Drop, for example from desktop to the canvas,\n is enabled. There is easy to use drag and drop scrapbook for frequently\n used items such as text blocks, pictures and custom shaped frames.\n .\n If you need to use the render frame install the texlive-latex-recommended\n (suggested).\n"
    fr:
      description: "application de PAO libre —\_branche stable"
      long_description: " Scribus est une application de mise en page (PAO) libre, dont l'objectif\n est de produire des documents de qualité professionnelle aux formats PDF\n et\n Postscript, principalement mais non exclusivement pour Linux.\n .\n Scribus peut être utilisé pour de nombreuses tâches\_: la conception de\n brochures, journaux, magazines, lettres et affiches ainsi que de la\n documentation technique. Il possède un système de mise en page\n sophistiquée\n incluant le placement précis et la rotation du texte et/ou des\n images, le réglage manuel de l'approche des caractères, les polygones à\n courbes de Bézier, le placement précis des objets, les calques et\n une gestion personnalisée des couleurs RVB et CMJN. Les documents Scribus\n sont enregistrés en XML. Contrairement aux formats de fichiers binaires\n propriétaires, même des documents endommagés peuvent être récupérés avec\n un simple éditeur de texte.\n .\n Scribus gère les fonctions PAO professionnelles\_: les couleurs en CMJN et\n un système de gestion des couleurs de l’image pour une haute qualité lors\n de l'impression, des options flexibles de création de PDF, l'import depuis et l'export vers du PostScript encapsulé et la séparation en\n 4\_couleurs, l'import d’EPS/PS et du SVG comme graphismes vectoriels\n natifs, le texte Unicode incluant les écritures de droite à gauche comme\n l'arabe et l'hébreu à l’aide de freetype. Les formats graphiques qui\n peuvent être incorporés dans Scribus en tant qu'image comprennent le PDF,\n l'EPS (Encapsulated PostScript), le TIFF, le JPEG, le PNG et le XPM\n (XPixMap) ainsi que n'importe quel type bitmap pris en charge par QT4.\n .\n L'impression et la création de PDF et SVG sont réalisées par des\n bibliothèques de pilotes et des greffons adaptés, donnant à Scribus des\n fonctionnalités ingénieuses\_: la possibilité d'inclure des effets de\n présentation avec la sortie PDF, des formulaires PDF interactifs\n entièrement programmables, la sortie en SVG. Les pilotes d'impression\n internes gèrent complètement les fonctionnalités postscript des niveaux\_2\n ou\_3 et PDF\_1.4, comme la transparence et l'inclusion de polices.\n .\n Lorsque Scribus est utilisé sous KDE, le glisser-déposer est actif (par\n exemple, du bureau au plan de travail). Cette facilité existe pour les\n éléments couramment utilisés comme les blocs de texte, les images et les\n formes personnalisées.\n .\n Si vous avez besoin de cadre de rendu, installez le paquet\n texlive-latex-recommended (suggestion).\n"
    it:
      description: impaginazione open source per il desktop - ramo stabile
      long_description: " Scribus è un programma open source di impaginazione per il desktop che mira\n a produrre output di qualità professionale in PDF e PostScript\n principalmente, ma non solamente, per Linux.\n .\n Scribus può essere usato per molti compiti; dalla creazione di brochure a\n quotidiani, riviste, bollettini e poster, fino a documentazione tecnica. Ha\n funzionalità sofisticate di impaginazione come posizionamento e\n rotazione precisi di testi o immagini nella pagina, crenatura manuale,\n poligoni con curve di Bézier, posizionamento preciso di oggetti,\n stratificazioni con colori personalizzati RGB e CMYK. Il formato dei file\n documento di Scribus è basato sull'XML. A differenza dei formati binari\n proprietari, anche i documenti danneggiati possono essere\n recuperati con un semplice editor di testo.\n .\n Scribus supporta funzionalità DTP professionali, come colori CMYK e un\n sistema di gestione dei colori per avere anteprime fedeli per stampa a\n colori di alta qualità, opzioni per la creazione flessibile di PDF,\n importazione ed esportazione di Encapsulated Postscript e creazione di\n versioni con separazione dei 4 colori, importazione di EPS/PS e SVG come\n elementi grafici vettoriali nativi, testi in Unicode compresi scritture da\n destra a sinistra come arabo e ebraico grazie a freetype. I formati grafici\n che possono essere usati in Scribus come immagini includono PDF,\n Encapsulated Post Script (eps), TIFF, JPEG, PNG e XPixMap(xpm) e qualsiasi\n tipo bitmap gestito da QT4.\n .\n La stampa e la creazione di PDF e SVG sono realizzate tramite librerie\n driver e plugin personalizzati e ciò dà a Scribus funzionalità ingegnose:\n la capacità di includere effetti di presentazione nell'output PDF, moduli\n PDF interattivi totalmente gestibili da script, output in file vettoriali\n SVG. I driver di stampa interni supportano pienamente le funzionalità\n PostScript Level 2, Level 3 e PDF 1.4 incluse la trasparenza e\n l'incorporazione di tipi di carattere.\n .\n Quando viene eseguito in KDE, è abilitato il drag&drop, per esempio tra il\n desktop e la pagina. C'è un comodo taccuino per il drag&drop per gli\n elementi usati di frequente, come blocchi di testo, immagini e riquadri di\n forma personalizzata.\n .\n Se si ha bisogno del riquadro di rendering, si installi\n texlive-latex-recommended (suggerito).\n"
    ko:
      description: 온픈 소스 데스크탑 페이지 레이아웃 - 안정 브랜치
      long_description: " Scribus는 리눅스용으로 독점적이지 않음에도 불구하고 PDF 및 포스트스크립트에\n 서 상용 제품 품질의 출력을 만드는 것이 주요 목표인 오픈소스 데스크탑 페이지\n 레이아웃 프로그램입니다.\n .\n Scribus는 신문, 잡지, 뉴스레터 그리고 포스터 팜플렛 작성에서 부터 기술 문선\n 까지 많은 작업에 사용될 수 있습니다. Scribus는 페이지상의 텍스트나 이미지의\n 정확한 배치 및 회전, 활자의 수동 돌출부, 베지에 곡선 다각형, 오브젝트의 정\n 확한 배치, RGB 및 CMYK 커스텀 색상을 갖는 다채색 표시같은 정교한 페이지 레\n 이아웃 기능을 가지고 있습니다. Scribus 문서 파일 형식은 XML 기반입니다. 독\n 점적 바이너리 파일 형식과 달리 문서가 파손되었을 때도, 간단한 텍스트 편집기\n 를 이용해서 복원할 수가 있습니다.\n .\n Sscibus는 고품질 컬러 프린팅을 위한 soft proof 이미지에 대한 CMYK 색상 및\n 색상 관리 시스템, 유연한 PDF 작성 옵션, Encapsulated PostScript(EPS)\n import/export 및 4 색분할의 작성, 네가티브 벡터 그래픽으로 EPS/PS 및 SVG의\n import, freetype을 통한 아라비아와 헤브루 문자처럼 오른쪽에서 왼쪽으로 된\n 활자를 포함하는 유니코드 텍스트와 같은 전문 DTP 기능을 지원합니다. 이미지로\n Scribus에 배치 가능한 그래픽 포맷에는 PDF, EPS, TIFF, JPEB, PNG 와\n XpixMap(XPM), 그리고 QT4에서 지원하는 모든 종류의 비트맵을 포함합니다.\n .\n 인쇄, PDF 및 SVG 작성은 커스텀 드라이버 라이브러리와 플러그인, 주어진\n Scribus의 현신적인 특징에 의해서 실행됩니다: PDF 출력과 함께 프리젠테이션\n 효과를 포함하는 능력, 완전히 스크립트 가능한 대화형 PDF 폼, SVG 벡터 파일\n 출력. 내부 프린터 드라이버는 투명화 및 내장 폰트를 포함하는 Level 2 및\n Level 3/PDF 1.4 포스트스크립트 기능을 완전히 지원합니다.\n .\n KDE에서 실행시, 드래그-앤-드롭, 예를 들면, 데스크탑에서 캔버스로 드래그-앤-\n 드롭이 가능합니다. 텍스트 블록, 사진 및 커스텀 형상 프레임 처럼 자주 사용되\n 는 아이템을 위해 사용하기 쉬운 드래그-앤-드롭 스크랩북이 있습니다.\n .\n 프레임 렌더 사용이 필요하면, texlive-latex-recommended 를 설치하십시요 (권장).\n"
    pl:
      description: Otwartoźródłowe oprogramowanie do składania tekstu - gałąź stabilna
      long_description: " Scribus jest otwartoźródłowym oprogramowaniem do składania tekstu, którego\n celem jest opracowanie dokumentów do poziomu komercyjnego w formatach PDF\n i Postscript, głównie lecz nie wyłącznie dla Linuksa.\n .\n Scribus może być używany do wielu zadań: od projektowania broszur, gazet,\n magazynów, biuletynów i plakatów, aż po dokumentację techniczną. Posiada\n wyszukane opcje składania stron tj. precyzyjnego przemieszczania oraz\n rotacji tekstu i obrazów na stronie, regulacji odstępów między znakami,\n obsługi poligonalnych krzywych Béziera, precyzyjnego umiejscawiania\n obiektów, współpracy z kolorami RGB i CMYK. Format zapisu plików Scribusa\n jest oparty na XML-u. W odróżnieniu od własnościowych formatów plików,\n nawet uszkodzone pliki mogą zostać odtworzone za pomocą prostego edytora\n tekstu.\n .\n Scribus obsługuje profesjonalne funkcje DTP, takie jak: paletę CMYK i\n system zarządzania kolorami do łagodnego poprawiania zdjęć w celu uzyskania\n wysokiej jakości wydruku, elastyczne opcje tworzenia PDF, import/eksport\n Encapsulated PostScript oraz tworzenie 4 separacji kolorów, import EPS/PS i\n SVG jako natywnych grafik wektorowych, dołączanie skryptów tekstowych\n pisanych w Unikodzie od prawej do lewej strony tak jak w języku arabskim\n czy hebrajskim przy użyciu freetype. Graficzne formaty mogą zostać\n umieszczone w Scribusie jako obrazy, dotyczy to również PDF, Encapsulated\n PostScript (eps), TIFF, JPEG, PNG i XPixMap (xpm) oraz każdego innego typu\n mapy bitowej obsługiwanej przez QT4.\n .\n Drukowanie oraz tworzenie PDF i SVG jest wykonywane za pomocą bibliotek\n niestandardowych sterowników i wtyczek, przez co Scribus posiada\n innowacyjne funkcje: możliwość dołączenia efektów prezentacji do\n wyjściowego pliku PDF, w pełni interaktywne formularze wykorzystujące\n skrypty, zapisywanie plików wyjściowych jako wektorowych plików SVG.\n Wewnętrzne sterowniki drukarki w pełni wspierają Level 2 i Level 3/PDF\n 1.4 funkcji PostScript obejmujących obsługę przezroczystości i osadzania\n czcionek.\n .\n Operacja przeciągnij i upuść np. z pulpitu do kanwy jest dostępna, gdy\n jest on uruchomiony w KDE. Dostępna jest również biblioteka korzystająca z\n łatwej w użyciu techniki przeciągnij i upuść z najczęściej używanymi\n opcjami takimi jak tekst, obrazy czy dowolnego kształtu ramki.\n .\n Jeśli zaistnieje potrzeba renderowania klatek należy zainstalować pakiet\n texlive-latex-recommended (sugerowane).\n"
    ru:
      description: издательская система с открытым исходным кодом — стабильная ветка
      long_description: " Scribus — свободная программа для вёрстки и макетирования, предназначенная\n для создания высококачественных публикаций в формате PDF и Postscript,\n преимущественно, но не исключительно, для платформ Linux.\n .\n Scribus можно использовать для решения различных задач: от дизайна брошюр\n до вёрстки газет, бюллетеней и плакатов и подготовки технической\n документации. В программе реализованы сложные функции макетирования вроде\n точного размещения объектов и вращения текста и изображений на странице,\n ручного кернинга текста, кривых Безье, слоёв. Формат документов Scribus\n использует язык описания XML, поэтому, в отличие от других программ, даже\n повреждённый файл может быть восстановлен в обычном текстовом редакторе.\n .\n В Scribus реализованы необходимые профессионалам функции настольного\n издательства, такие как цветовое пространство CMYK и система управления\n цветом программной цветопробы для высококачественно печати, гибкие\n параметры создания PDF, импорт и экспорт Encapsulated PostScript с\n созданием четырёх цветоделений, импорт данных EPS/PS и SVG как собственных\n векторных объектов, текст Unicode, включая письменности справа налево,\n такие как арабская и иврит (при помощи FreeType). В качестве изображений,\n для импорта в Scribus, можно использовать форматы PDF, Encapsulated Post\n Script (EPS), TIFF, JPEG, PNG и XPixMap (XPM), а также все форматы,\n поддерживаемые Qt4.\n .\n Печать и создание PDF и SVG реализованы при помощи собственных библиотек и\n расширений. Так, вы можете создавать презентации в PDF с эффектами\n перехода между слайдами, интерактивные формуляры PDF и файлы SVG.\n Встроенные драйверы печати поддерживают вывод на Postscript Level 2 и\n Level 3, а также PDF 1.4 с поддержкой прозрачности и встраиванием шрифтов.\n .\n Если программа запущена в KDE, можно перетаскивать мышкой файлы прямо на\n холст программы. Часто используемые элементы вроде текстовых блоков,\n изображений и блоков произвольной формы доступны для перетаскивания в\n специальной области.\n .\n Если вам требуется блок визуализации, то установите пакет\n texlive-latex-recommended.\n"
  screenshot:
    large_image_url: 'http://screenshots.debian.net/screenshots/s/scribus/9959_large.png'
    screenshot_url: 'http://screenshots.debian.net/package/scribus'
    small_image_url: 'http://screenshots.debian.net/screenshots/s/scribus/9959_small.png'
debian_appstream:
  _source:
    name: Debian Appstream
    description: 'https://wiki.debian.org/AppStream'
    url: 'https://git.framasoft.org/codegouv/appstream-debian-yaml'
  Categories:
    - Qt
    - Graphics
    - Publishing
  Description:
    C: '<p>Open Source Desktop Page Layout - stable branch</p>'
  ID: vnd.scribus.desktop
  Icon:
    cached: scribus_scribus.png
    stock: scribus
  Name:
    C: Scribus
  Package: scribus
  Provides:
    mimetypes:
      - application/vnd.scribus
  Summary:
    C: Page Layout and Publication
    af: Werkskerm Publisering
    ar: سكريبس
    bg: Издателска програма
    bs: Stono izdavaštvo (DTP)
    ca: "Publicació d'escriptori"
    cs: Publikační systém (DTP)
    cy: Cyhoeddi Penbwrdd
    da: Dtp
    eo: DTP-programo
    es: Publicación de escritorio
    et: Küljendusrakendus
    fa: اسکریباس
    fi: Julkaisuohjelma
    fr: Publication assistée par ordinateur (PAO)
    he: הוצאה לאור שולחנית
    hr: Stolno izdavaštvo
    hu: Kiadványszerkesztés
    it: Pubblicazioni
    ja: デスクトップパブリッシング
    lo: ເດດທອບພັລບບີດຊິງ
    lt: Darbastalio leidykla
    nb: Datatrykkeprogram
    nn: Datatrykk
    nso: Kwalakwatso ya Desktop
    pl: Program do składu tekstu
    pt: Publicação no Ecrã
    pt_br: Publicação do Ambiente de Trabalho
    ru: Настольное издательство
    sl: Namizno založništvo
    sr: Стоно издаваштво
    ss: Kushicelela ku desktop
    sv: Desktop Publishing
    ta: ஸ்கிரிபஸ்
    th: เดสก์ทอปพับลิชชิง
    tr: Masaüstü Yayıncılık
    uk: Видавнича система
    uz: Кичик нашриёт
    ven: U andadza Desikithopo
    xh: Upapasho lwe Desktop
    zh_cn: 桌面出版
    zh_tw: 桌面出版
    zu: Ukushicilelwa kwe-Desktop
  Type: desktop-app
wikidata:
  _source:
    name: Wikidata
    description: 'http://wikidata.org/'
    url: 'https://git.framasoft.org/codegouv/wikidata-yaml'
  bug_tracker:
    - type: uri
      value: 'http://bugs.scribus.net'
  freebase:
    - type: literal
      value: /m/01qjvn
  image:
    - type: uri
      value: 'http://commons.wikimedia.org/wiki/Special:FilePath/Scribus-1.3-Linux.png'
  license:
    - type: uri
      value: 'http://www.wikidata.org/entity/Q7603'
  license_enlabel:
    - type: literal
      value: GNU General Public License
      'xml:lang': en
  license_eslabel:
    - type: literal
      value: GNU General Public License
      'xml:lang': es
  license_frlabel:
    - type: literal
      value: Licence publique générale GNU
      'xml:lang': fr
  logotype:
    - type: uri
      value: 'http://commons.wikimedia.org/wiki/Special:FilePath/Scribus%20logo.svg'
  os:
    - type: uri
      value: 'http://www.wikidata.org/entity/Q388'
    - type: uri
      value: 'http://www.wikidata.org/entity/Q1406'
    - type: uri
      value: 'http://www.wikidata.org/entity/Q11368'
    - type: uri
      value: 'http://www.wikidata.org/entity/Q34264'
    - type: uri
      value: 'http://www.wikidata.org/entity/Q14116'
    - type: uri
      value: 'http://www.wikidata.org/entity/Q14646'
    - type: uri
      value: 'http://www.wikidata.org/entity/Q189794'
  os_enlabel:
    - type: literal
      value: Linux
      'xml:lang': en
    - type: literal
      value: Microsoft Windows
      'xml:lang': en
    - type: literal
      value: Unix
      'xml:lang': en
    - type: literal
      value: Berkeley Software Distribution
      'xml:lang': en
    - type: literal
      value: OS X
      'xml:lang': en
    - type: literal
      value: Solaris
      'xml:lang': en
    - type: literal
      value: OS/2
      'xml:lang': en
  os_eslabel:
    - type: literal
      value: Linux
      'xml:lang': es
    - type: literal
      value: Microsoft Windows
      'xml:lang': es
    - type: literal
      value: Unix
      'xml:lang': es
    - type: literal
      value: Berkeley Software Distribution
      'xml:lang': es
    - type: literal
      value: OS X
      'xml:lang': es
    - type: literal
      value: Solaris
      'xml:lang': es
    - type: literal
      value: OS/2
      'xml:lang': es
  os_frlabel:
    - type: literal
      value: Linux
      'xml:lang': fr
    - type: literal
      value: Microsoft Windows
      'xml:lang': fr
    - type: literal
      value: Unix
      'xml:lang': fr
    - type: literal
      value: Berkeley Software Distribution
      'xml:lang': fr
    - type: literal
      value: OS X
      'xml:lang': fr
    - type: literal
      value: Solaris
      'xml:lang': fr
    - type: literal
      value: OS/2
      'xml:lang': fr
  program:
    - type: uri
      value: 'http://www.wikidata.org/entity/Q8295'
  program_enlabel:
    - type: literal
      value: Scribus
      'xml:lang': en
  program_eslabel:
    - type: literal
      value: Scribus
      'xml:lang': es
  program_frlabel:
    - type: literal
      value: Scribus
      'xml:lang': fr
  programming_langage:
    - type: uri
      value: 'http://www.wikidata.org/entity/Q2407'
  versions:
    - type: literal
      value: 1.4.6
  website:
    - type: uri
      value: 'http://www.scribus.net'
