name: freeplane
canonical:
  canonicalName:
    source: debian_appstream
    value: Freeplane
  categories:
    - source: debian_appstream
      value: Office
  license:
    source: wikidata
    value: GNU General Public License
  longDescription:
    en:
      source: debian
      value: " Freeplane is a free and open source software application that supports\n thinking, sharing information and getting things done at work, in school\n and at home. The core of the software is tools for mind mapping (also\n known as concept mapping or information mapping) and using mapped\n information.\n .\n Occupying the middle ground between an editor and a diagramming tool,\n Freeplane allows the user to add content as quickly and naturally as they\n would in a text editor, yet producing structured content that can be\n manipulated as easily as a diagram.\n .\n Features include ordering ideas in nodes and freely positionable\n nodes, connecting nodes, automatic/conditional styles, scripting,\n add-ons, LaTeX, search/filtering, different export features, printing,\n password protection of nodes/maps and more.\n .\n See http://freeplane.sourceforge.net/wiki/index.php/Main_Page for a full\n list of applications and features.\n"
  screenshot:
    source: debian
    value: 'http://screenshots.debian.net/screenshots/f/freeplane/9195_large.png'
debian:
  _source:
    name: Universal Debian Database
    description_url: 'https://wiki.debian.org/UltimateDebianDatabase'
    url: 'https://git.framasoft.org/codegouv/udd-yaml'
  name: freeplane
  description:
    de:
      description: Java-Programm für die Arbeit mit Mind Maps
      long_description: " Freeplane ist eine freie und quelloffene Anwendung. Sie unterstützt das\n Denken, den Austausch von Informationen und die Erledigung der Aufgaben bei\n der Arbeit, in der Schule und zu Hause. Der Kern der Software sind Werkzeuge\n zum Mind-Mapping (auch bekannt als Concept-Mapping oder Information-Mapping)\n und zur Verwendung der enthaltenen Informationen.\n .\n Freeplane beansprucht die Mitte zwischen einem Editor und einem\n Diagrammwerkzeug für sich. Das Programm ermöglicht dem Benutzer, Inhalte so\n schnell und natürlich wie mit einem Texteditor einzufügen und trotzdem\n strukturierte Inhalte zu erstellen, die ebenso leicht wie ein Diagramm\n manipuliert werden können.\n .\n Zu den Eigenschaften gehören die Anordnung von Gedanken in Knoten und frei\n positionierbaren Knoten, das Verbinden von Knoten, automatische/bedingte\n Stile, Scripting, Add-ons, LaTeX, Suchen/Filtern, verschiedene\n Exportfunktionen, Drucken, Passwortschutz von Knoten/Maps und mehr.\n .\n Siehe http://freeplane.sourceforge.net/wiki/index.php/Main_Page für eine\n vollständige Liste von Anwendungen und Funktionen.\n"
    en:
      description: Java program for working with Mind Maps
      long_description: " Freeplane is a free and open source software application that supports\n thinking, sharing information and getting things done at work, in school\n and at home. The core of the software is tools for mind mapping (also\n known as concept mapping or information mapping) and using mapped\n information.\n .\n Occupying the middle ground between an editor and a diagramming tool,\n Freeplane allows the user to add content as quickly and naturally as they\n would in a text editor, yet producing structured content that can be\n manipulated as easily as a diagram.\n .\n Features include ordering ideas in nodes and freely positionable\n nodes, connecting nodes, automatic/conditional styles, scripting,\n add-ons, LaTeX, search/filtering, different export features, printing,\n password protection of nodes/maps and more.\n .\n See http://freeplane.sourceforge.net/wiki/index.php/Main_Page for a full\n list of applications and features.\n"
    it:
      description: programma Java per lavorare con mappe mentali
      long_description: " Freeplane è un'applicazione software libera e open source che aiuta a\n pensare, condividere informazioni e portare a termine compiti sul lavoro, a\n scuola e a casa. Il nucleo centrale del software è formato da strumenti per\n la creazione di mappe mentali (nota anche come mappatura dei contenuti o\n mappatura delle informazioni) e per usare le informazioni mappate.\n .\n Essendo una via di mezzo tra un editor e uno strumento per diagrammi,\n Freeplane permette anche di aggiungere contenuti in modo tanto veloce e\n naturale quanto in un editor di testi, producendo però contenuti\n strutturati che possono essere manipolati tanto facilmente quanto un diagramma.\n .\n Le funzionalità includono l'ordinamento delle idee in nodi e nodi\n liberamente posizionabili, connessione di nodi, stili\n automatici/condizionali, script, moduli aggiuntivi, LaTeX, ricerca e\n filtri, diverse funzionalità di esportazione, stampa, protezione tramite\n password di nodi o mappe e altro ancora.\n .\n Per un elenco completo delle applicazioni e delle caratteristiche vedere\n http://freeplane.sourceforge.net/wiki/index.php/Main_Page\n"
  screenshot:
    large_image_url: 'http://screenshots.debian.net/screenshots/f/freeplane/9195_large.png'
    screenshot_url: 'http://screenshots.debian.net/package/freeplane'
    small_image_url: 'http://screenshots.debian.net/screenshots/f/freeplane/9195_small.png'
debian_appstream:
  _source:
    name: Debian Appstream
    description: 'https://wiki.debian.org/AppStream'
    url: 'https://git.framasoft.org/codegouv/appstream-debian-yaml'
  Categories:
    - Office
  Description:
    C: '<p>Java program for working with Mind Maps</p>'
  ID: freeplane.desktop
  Icon:
    cached: freeplane_freeplane.png
  Keywords:
    C:
      - Mindmaps
      - ' Knowledge management'
      - ' Organize information'
      - ' Brainstorming'
      - ' ...'
  Name:
    C: Freeplane
  Package: freeplane
  Provides:
    mimetypes:
      - application/x-freeplane
  Summary:
    C: A free tool to structure and organise your information with mind mapping
  Type: desktop-app
wikidata:
  _source:
    name: Wikidata
    description: 'http://wikidata.org/'
    url: 'https://git.framasoft.org/codegouv/wikidata-yaml'
  freebase:
    - type: literal
      value: /m/09v200y
  image:
    - type: uri
      value: 'http://commons.wikimedia.org/wiki/Special:FilePath/Freeplane%20Mind-mapping.png'
  license:
    - type: uri
      value: 'http://www.wikidata.org/entity/Q7603'
  license_enlabel:
    - type: literal
      value: GNU General Public License
      'xml:lang': en
  license_eslabel:
    - type: literal
      value: GNU General Public License
      'xml:lang': es
  license_frlabel:
    - type: literal
      value: Licence publique générale GNU
      'xml:lang': fr
  logotype:
    - type: uri
      value: 'http://commons.wikimedia.org/wiki/Special:FilePath/Freeplane%20logo.png'
  program:
    - type: uri
      value: 'http://www.wikidata.org/entity/Q3028675'
  program_enlabel:
    - type: literal
      value: Freeplane
      'xml:lang': en
  program_eslabel:
    - type: literal
      value: Freeplane
      'xml:lang': es
  program_frlabel:
    - type: literal
      value: Freeplane
      'xml:lang': fr
  programming_langage:
    - type: uri
      value: 'http://www.wikidata.org/entity/Q251'
  website:
    - type: uri
      value: 'http://freeplane.sourceforge.net/'
